package com.kian.film.domain

data class Language(
    val iso6391: String,
    val name: String
)
