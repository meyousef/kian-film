package com.kian.film.data

import com.kian.film.domain.MovieDetails
import com.kian.film.domain.MovieList
import com.kian.film.domain.Resource

interface MovieDataSource {

    suspend fun getNowPlayingMovies(page: Int): Resource<MovieList>

    suspend fun getPopularMovies(page: Int): Resource<MovieList>

    suspend fun getTopRatedMovies(page: Int): Resource<MovieList>

    suspend fun getUpcomingMovies(page: Int): Resource<MovieList>

    suspend fun getMovieDetails(movieId: Int): Resource<MovieDetails>
}