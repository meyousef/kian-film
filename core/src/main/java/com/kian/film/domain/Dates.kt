package com.kian.film.domain

data class Dates(
    val maximum: String,
    val minimum: String
)
