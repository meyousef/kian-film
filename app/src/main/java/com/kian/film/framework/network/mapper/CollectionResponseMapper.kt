package com.kian.film.framework.network.mapper

import com.kian.film.domain.Collection
import com.kian.film.mapper.DataMapper
import com.kian.film.framework.network.response.CollectionResponse
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CollectionResponseMapper @Inject constructor() : DataMapper<Collection, CollectionResponse>() {

    override fun transformToEntity(model: Collection): CollectionResponse? {
        // Unnecessary transform
        return null
    }

    override fun transformToModel(entity: CollectionResponse): Collection? {
        return Collection(
            entity.backdrop_path,
            entity.id,
            entity.name,
            entity.poster_path
        )
    }
}
