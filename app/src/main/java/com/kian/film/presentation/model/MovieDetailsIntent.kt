package com.kian.film.presentation.model

data class MovieDetailsIntent(val movieId: Int, val movieName: String?) {

    override fun toString(): String {
        return """UserPresenceIntent: 
                ID   = $movieId, 
                Name = $movieName"""
    }
}
