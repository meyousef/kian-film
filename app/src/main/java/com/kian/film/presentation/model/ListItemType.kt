package com.kian.film.presentation.model

enum class ListItemType(val value: Int) {

    MOVIE_ITEM(0), LOADING_ITEM(1), ERROR_ITEM(2);

    companion object {
        fun ofValue(value: Int): ListItemType? {
            for (userIssue in values()) {
                if (userIssue.value == value) {
                    return userIssue
                }
            }

            return null
        }
    }
}
