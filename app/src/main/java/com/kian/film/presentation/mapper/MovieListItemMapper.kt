package com.kian.film.presentation.mapper

import com.kian.film.domain.Movie
import com.kian.film.mapper.DataMapper
import com.kian.film.presentation.model.ListItem
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MovieListItemMapper @Inject constructor() : DataMapper<Movie, ListItem.MovieListItem>() {

    override fun transformToEntity(model: Movie): ListItem.MovieListItem? {
        return ListItem.MovieListItem(model)
    }

    override fun transformToModel(entity: ListItem.MovieListItem): Movie? {
        // Unnecessary transform
        return null
    }
}
