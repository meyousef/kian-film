package com.kian.film.framework.network.mapper

import com.kian.film.domain.Company
import com.kian.film.mapper.DataMapper
import com.kian.film.framework.network.response.CompanyResponse
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CompanyResponseMapper @Inject constructor() : DataMapper<Company, CompanyResponse>() {

    override fun transformToEntity(model: Company): CompanyResponse? {
        // Unnecessary transform
        return null
    }

    override fun transformToModel(entity: CompanyResponse): Company? {
        return Company(
            entity.id,
            entity.logo_path,
            entity.name,
            entity.origin_country
        )
    }
}
