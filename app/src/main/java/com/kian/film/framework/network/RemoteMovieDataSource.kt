package com.kian.film.framework.network

import android.util.Log
import com.kian.film.data.MovieDataSource
import com.kian.film.domain.MovieDetails
import com.kian.film.domain.MovieList
import com.kian.film.domain.Resource
import com.kian.film.framework.network.mapper.DetailsOfMovieResponseMapper
import com.kian.film.framework.network.mapper.ListOfMoviesResponseMapper
import java.io.IOException
import javax.inject.Inject

class RemoteMovieDataSource @Inject constructor(
    private val service: RemoteMovieService,
    private val listOfMoviesResponseMapper: ListOfMoviesResponseMapper,
    private val detailsOfMovieResponseMapper: DetailsOfMovieResponseMapper
) : MovieDataSource {

    override suspend fun getNowPlayingMovies(page: Int): Resource<MovieList> {
        return try {
            val response = service.fetchNowPlayings(page)
            return if (response.isSuccessful) {
                Resource.success(listOfMoviesResponseMapper.transformToModel(response.body()!!)!!)
            }
            else {
                /* Handle standard error codes, by checking [response.code()] */

                Resource.error(
                    IOException(response.errorBody()?.string() ?: "Something goes wrong")
                )
            }
        }
        catch (e: Exception) {
            // DEBUG
            Log.e("getNowPlayingMovies", e.message ?: "Internet error runs")

            Resource.error(IOException(e.message ?: "Internet error runs"))
        }
    }

    override suspend fun getPopularMovies(page: Int): Resource<MovieList> {
        return try {
            val response = service.fetchPopulars(page)
            return if (response.isSuccessful) {
                Resource.success(listOfMoviesResponseMapper.transformToModel(response.body()!!)!!)
            }
            else {
                /* Handle standard error codes, by checking [response.code()] */

                Resource.error(
                    IOException(response.errorBody()?.string() ?: "Something goes wrong")
                )
            }
        }
        catch (e: Exception) {
            // DEBUG
            Log.e("getPopularMovies", e.message ?: "Internet error runs")

            Resource.error(IOException(e.message ?: "Internet error runs"))
        }
    }

    override suspend fun getTopRatedMovies(page: Int): Resource<MovieList> {
        return try {
            val response = service.fetchTopRates(page)
            return if (response.isSuccessful) {
                Resource.success(listOfMoviesResponseMapper.transformToModel(response.body()!!)!!)
            }
            else {
                /* Handle standard error codes, by checking [response.code()] */

                Resource.error(
                    IOException(response.errorBody()?.string() ?: "Something goes wrong")
                )
            }
        }
        catch (e: Exception) {
            // DEBUG
            Log.e("getTopRatedMovies", e.message ?: "Internet error runs")

            Resource.error(IOException(e.message ?: "Internet error runs"))
        }
    }

    override suspend fun getUpcomingMovies(page: Int): Resource<MovieList> {
        return try {
            val response = service.fetchUpcomings(page)
            return if (response.isSuccessful) {
                Resource.success(listOfMoviesResponseMapper.transformToModel(response.body()!!)!!)
            }
            else {
                /* Handle standard error codes, by checking [response.code()] */

                Resource.error(
                    IOException(response.errorBody()?.string() ?: "Something goes wrong")
                )
            }
        }
        catch (e: Exception) {
            // DEBUG
            Log.e("getUpcomingMovies", e.message ?: "Internet error runs")

            Resource.error(IOException(e.message ?: "Internet error runs"))
        }
    }

    override suspend fun getMovieDetails(movieId: Int): Resource<MovieDetails> {
        return try {
            val response = service.fetchDetailsOf(movieId)
            return if (response.isSuccessful) {
                Resource.success(detailsOfMovieResponseMapper.transformToModel(response.body()!!)!!)
            }
            else {
                /* Handle standard error codes, by checking [response.code()] */

                Resource.error(
                    IOException(response.errorBody()?.string() ?: "Something goes wrong")
                )
            }
        }
        catch (e: Exception) {
            // DEBUG
            Log.e("getMovieDetails", e.message ?: "Internet error runs")

            Resource.error(IOException(e.message ?: "Internet error runs"))
        }
    }
}
