package com.kian.film.framework.network.mapper

import com.kian.film.domain.Language
import com.kian.film.mapper.DataMapper
import com.kian.film.framework.network.response.LanguageResponse
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LanguageResponseMapper @Inject constructor() : DataMapper<Language, LanguageResponse>() {

    override fun transformToEntity(model: Language): LanguageResponse? {
        // Unnecessary transform
        return null
    }

    override fun transformToModel(entity: LanguageResponse): Language? {
        return Language(
            entity.iso_639_1,
            entity.name
        )
    }
}
