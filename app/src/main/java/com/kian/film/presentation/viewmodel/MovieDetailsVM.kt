package com.kian.film.presentation.viewmodel

import android.content.Intent
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.kian.film.Const
import com.kian.film.common.help.RxAwareViewModel
import com.kian.film.domain.MovieDetails
import com.kian.film.domain.Resource
import com.kian.film.domain.Status
import com.kian.film.interactors.GetMovieDetails
import com.kian.film.presentation.model.MovieDetailsIntent
import com.kian.film.presentation.viewstate.MovieDetailsVS
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

class MovieDetailsVM @Inject constructor(
    private val getMovieDetails: GetMovieDetails
) : RxAwareViewModel() {

    private val movieDetailsIntent = MutableLiveData<MovieDetailsIntent>()
    private val movieDetailsStream = MutableLiveData<MovieDetailsVS>()

    fun getMovieDetailsIntent(): LiveData<MovieDetailsIntent> = movieDetailsIntent
    fun getMovieDetailsStream(): LiveData<MovieDetailsVS> = movieDetailsStream

    fun onNewIntent(data: Intent?) {
        data?.let {
            movieDetailsIntent.postValue(
                MovieDetailsIntent(
                    it.getIntExtra(Const.EXTRA_MOVIE_ID, 0),
                    it.getStringExtra(Const.EXTRA_MOVIE_NAME)
                )
            )
        }
    }

    fun fetchMovieDetails(movieId: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            val resource = getMovieDetails.invoke(movieId)
            withContext(Dispatchers.Main) {
                movieDetailsStream.postValue(transformResourceToViewState(resource))
            }
        }

        // Dispatch loading view state
        movieDetailsStream.postValue(buildLoadingViewState())
    }

    private fun transformResourceToViewState(resource: Resource<MovieDetails>): MovieDetailsVS {
        return MovieDetailsVS(
            resource.status,
            resource.error?.message?.let { buildHumanReadableErrorMessage(it) },
            resource.data
        )
    }

    private fun buildLoadingViewState(): MovieDetailsVS {
        return MovieDetailsVS(status = Status.LOADING)
    }

    /**
     * Add some magics here to make error messages more user-friendly... 😊
     */
    private fun buildHumanReadableErrorMessage(message: String): String {
        return "Unable to load details of movie! Please check your internet and then swipe to refresh."
    }
}
