package com.kian.film.presentation.view.ui

import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.view.Menu
import android.view.animation.AnimationUtils
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.kian.film.R
import com.kian.film.common.BaseActivity
import com.kian.film.common.BaseFragment
import com.kian.film.common.help.observeNonNull
import com.kian.film.databinding.ActivityMovieDetailsBinding
import com.kian.film.presentation.viewmodel.MovieDetailsVM
import com.kian.film.presentation.viewstate.MovieDetailsVS
import javax.inject.Inject

class MovieDetailsActivity : BaseActivity() {

    /**
     * Values
     */

    @Inject
    internal lateinit var viewModelProviderFactory: ViewModelProvider.Factory

    private lateinit var movieDetailsVM: MovieDetailsVM
    private lateinit var dataBinding: ActivityMovieDetailsBinding

    /****************************************************
     * ACTIVITY LIFECYCLE
     ***************************************************/

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
    }

    override fun onStop() {
        super.onStop()
    }

    override fun onRestart() {
        super.onRestart()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)

        extractIntentParams(intent)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_movie_details, menu)

        return true
    }

    /****************************************************
     * ACTIVITY STATE
     ***************************************************/

    override fun initializeActivity(savedInstanceState: Bundle?) {
        // Nothing
    }

    override fun extractIntentParams(data: Intent?) {
        movieDetailsVM.onNewIntent(data)
    }

    /****************************************************
     * VIEW/DATA BINDING
     ***************************************************/

    override fun setupViews() {
        // Set Content View
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_movie_details)

        setupSwipeContainerView()
        setupErrorAnnounce()
    }

    override fun setupActionBar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        val actionBar = supportActionBar
        actionBar!!.setDisplayShowTitleEnabled(true)
        actionBar.setDisplayHomeAsUpEnabled(false)

        // Customize AppBar or other uncommon here
        this.supportActionBar!!.setTitle(R.string.movie_details_activity_label)
    }

    override fun setupNavigation() {
        // Nothing
    }

    private fun setupSwipeContainerView() {
        dataBinding.content.swipeContainer.setOnRefreshListener {
            movieDetailsVM.getMovieDetailsIntent().value?.let {
                fetchMovieDetails(it.movieId)
            }
        }
    }

    private fun setupErrorAnnounce() {
        dataBinding.content.errorAnnounce.setFactory {
            val t = TextView(applicationContext)
            t.gravity = Gravity.CENTER_VERTICAL or Gravity.CENTER_HORIZONTAL
            t.setTextColor(applicationContext!!.resources.getColor(R.color.colorTextDescription))
            t
        }
        dataBinding.content.errorAnnounce.inAnimation = AnimationUtils.loadAnimation(
            applicationContext,
            R.anim.slide_in_down
        )
        dataBinding.content.errorAnnounce.outAnimation = AnimationUtils.loadAnimation(
            applicationContext,
            R.anim.slide_out_up
        )
        dataBinding.content.errorAnnounce.setCurrentText("")
    }

    private fun renderMovieDetailsViewState(movieDetailsVS: MovieDetailsVS) {
        with(dataBinding.content) {
            viewState = movieDetailsVS
            executePendingBindings()
        }

        // Handle showing loading indicator
        dataBinding.content.swipeContainer.post {
            dataBinding.content.swipeContainer.isRefreshing = movieDetailsVS.isLoading()
        }

        // Handle showing error
        if (movieDetailsVS.shouldShowErrorMessage()) {
            dataBinding.content.errorAnnounce.setText(movieDetailsVS.getErrorMessage())
        }
        else {
            dataBinding.content.errorAnnounce.setText("")
        }
    }

    private fun fetchMovieDetails(movieId: Int) {
        movieDetailsVM.fetchMovieDetails(movieId)
    }

    /****************************************************
     * OBSERVERS
     ***************************************************/

    override fun setupObservers() {
        super.setupObservers()

        movieDetailsVM = ViewModelProvider(
            this,
            viewModelProviderFactory
        ).get(MovieDetailsVM::class.java)

        movieDetailsVM.getMovieDetailsIntent().observeNonNull(this) {
            fetchMovieDetails(it.movieId)
        }

        movieDetailsVM.getMovieDetailsStream().observeNonNull(this) {
            renderMovieDetailsViewState(it)
        }
    }

    /****************************************************
     * SERVICE BINDING
     ***************************************************/

    // Nothing

    companion object {
        val TAG = MovieDetailsActivity::class.java.simpleName
    }
}
