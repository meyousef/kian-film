package com.kian.film.domain

data class Company(
    val id: Int,
    val logoPath: String?,
    val name: String,
    val originCountry: String?
)
