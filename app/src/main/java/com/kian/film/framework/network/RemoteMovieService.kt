package com.kian.film.framework.network

import com.kian.film.framework.network.response.DetailsOfMovieResponse
import com.kian.film.framework.network.response.ListOfMoviesResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface RemoteMovieService {

    @GET("movie/now_playing")
    suspend fun fetchNowPlayings(@Query("page") page: Int): Response<ListOfMoviesResponse>

    @GET("movie/popular")
    suspend fun fetchPopulars(@Query("page") page: Int): Response<ListOfMoviesResponse>

    @GET("movie/top_rated")
    suspend fun fetchTopRates(@Query("page") page: Int): Response<ListOfMoviesResponse>

    @GET("movie/upcoming")
    suspend fun fetchUpcomings(@Query("page") page: Int): Response<ListOfMoviesResponse>

    @GET("movie/{movie_id}")
    suspend fun fetchDetailsOf(@Path("movie_id") movieId: Int): Response<DetailsOfMovieResponse>
}
