package com.kian.film.framework.network.mapper

import com.kian.film.domain.Dates
import com.kian.film.mapper.DataMapper
import com.kian.film.framework.network.response.DatesResponse
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DatesResponseMapper @Inject constructor() : DataMapper<Dates, DatesResponse>() {

    override fun transformToEntity(model: Dates): DatesResponse? {
        // Unnecessary transform
        return null
    }

    override fun transformToModel(entity: DatesResponse): Dates? {
        return Dates(
            entity.maximum,
            entity.minimum
        )
    }
}
