package com.kian.film.domain

data class Genre(
    val id: Int,
    val name: String
)
