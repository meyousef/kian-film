package com.kian.film.framework.network.mapper

import com.kian.film.domain.MovieDetails
import com.kian.film.mapper.DataMapper
import com.kian.film.framework.network.response.DetailsOfMovieResponse
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DetailsOfMovieResponseMapper @Inject constructor(
    private val collectionResponseMapper: CollectionResponseMapper,
    private val genreResponseMapper: GenreResponseMapper,
    private val companyResponseMapper: CompanyResponseMapper,
    private val countryResponseMapper: CountryResponseMapper,
    private val languageResponseMapper: LanguageResponseMapper
) : DataMapper<MovieDetails, DetailsOfMovieResponse>() {

    override fun transformToEntity(model: MovieDetails): DetailsOfMovieResponse? {
        // Unnecessary transform
        return null
    }

    override fun transformToModel(entity: DetailsOfMovieResponse): MovieDetails? {
        return MovieDetails(
            entity.adult,
            entity.backdrop_path,
            entity.belongs_to_collection?.let { collectionResponseMapper.transformToModel(it) },
            entity.budget,
            entity.genres?.let { genreResponseMapper.transformToModels(it) },
            entity.homepage,
            entity.id,
            entity.imdb_id,
            entity.original_language,
            entity.original_title,
            entity.overview,
            entity.popularity,
            entity.poster_path,
            entity.production_companies?.let { companyResponseMapper.transformToModels(it) },
            entity.production_countries?.let { countryResponseMapper.transformToModels(it) },
            entity.release_date,
            entity.revenue,
            entity.runtime,
            entity.spoken_languages?.let { languageResponseMapper.transformToModels(it) },
            entity.status,
            entity.tagline,
            entity.title,
            entity.video,
            entity.vote_average,
            entity.vote_count
        )
    }
}
