package com.kian.film.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.kian.film.common.help.RxAwareViewModel
import com.kian.film.domain.MovieList
import com.kian.film.domain.Resource
import com.kian.film.domain.Status
import com.kian.film.interactors.GetNowPlayingMovies
import com.kian.film.interactors.GetPopularMovies
import com.kian.film.interactors.GetTopRatedMovies
import com.kian.film.interactors.GetUpcomingMovies
import com.kian.film.presentation.mapper.MovieListItemMapper
import com.kian.film.presentation.viewstate.MovieListVS
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

class MainVM @Inject constructor(
    private val getNowPlayingMovies: GetNowPlayingMovies,
    private val getPopularMovies: GetPopularMovies,
    private val getTopRatedMovies: GetTopRatedMovies,
    private val getUpcomingMovies: GetUpcomingMovies,
    private val movieListItemMapper: MovieListItemMapper
) : RxAwareViewModel() {

    private val nowPlayingMoviesStream = MutableLiveData<MovieListVS>()
    private val popularMoviesStream = MutableLiveData<MovieListVS>()
    private val topRatedMoviesStream = MutableLiveData<MovieListVS>()
    private val upcomingMoviesStream = MutableLiveData<MovieListVS>()

    fun getNowPlayingMoviesStream(): LiveData<MovieListVS> = nowPlayingMoviesStream
    fun getPopularMoviesStream(): LiveData<MovieListVS> = popularMoviesStream
    fun getTopRatedMoviesStream(): LiveData<MovieListVS> = topRatedMoviesStream
    fun getUpcomingMoviesStream(): LiveData<MovieListVS> = upcomingMoviesStream

    fun fetchNowPlayingMovies(page: Int) {
        viewModelScope.launch {
            val resource = getNowPlayingMovies.invoke(page)
            withContext(Dispatchers.Main) {
                nowPlayingMoviesStream.postValue(transformResourceToViewState(resource, page))
            }
        }

        // Dispatch loading view state
        nowPlayingMoviesStream.postValue(buildLoadingViewState(page))
    }

    fun fetchPopularMovies(page: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            val resource = getPopularMovies.invoke(page)
            withContext(Dispatchers.Main) {
                popularMoviesStream.postValue(transformResourceToViewState(resource, page))
            }
        }

        // Dispatch loading view state
        popularMoviesStream.postValue(buildLoadingViewState(page))
    }

    fun fetchTopRatedMovies(page: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            val resource = getTopRatedMovies.invoke(page)
            withContext(Dispatchers.Main) {
                topRatedMoviesStream.postValue(transformResourceToViewState(resource, page))
            }
        }

        // Dispatch loading view state
        topRatedMoviesStream.postValue(buildLoadingViewState(page))
    }

    fun fetchUpcomingMovies(page: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            val resource = getUpcomingMovies.invoke(page)
            withContext(Dispatchers.Main) {
                upcomingMoviesStream.postValue(transformResourceToViewState(resource, page))
            }
        }

        // Dispatch loading view state
        upcomingMoviesStream.postValue(buildLoadingViewState(page))
    }

    private fun transformResourceToViewState(
        resource: Resource<MovieList>,
        page: Int
    ): MovieListVS {
        return MovieListVS(
            resource.status,
            resource.error?.message?.let { buildHumanReadableErrorMessage(it, page) },
            resource.data?.let { movieListItemMapper.transformToEntities(it.movies) },
            page
        )
    }

    private fun buildLoadingViewState(page: Int): MovieListVS {
        return MovieListVS(status = Status.LOADING, page = page)
    }

    /**
     * Add some magics here to make error messages more user-friendly... 😊
     */
    private fun buildHumanReadableErrorMessage(message: String, page: Int): String {
        return when (page) {
            1 -> "Unable to load any movie! Please check your internet and then swipe to refresh."
            else -> "Unable to load more movies! Tap to retry."
        }
    }
}
