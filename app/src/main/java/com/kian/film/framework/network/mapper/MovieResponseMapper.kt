package com.kian.film.framework.network.mapper

import com.kian.film.domain.Movie
import com.kian.film.mapper.DataMapper
import com.kian.film.framework.network.response.MovieResponse
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MovieResponseMapper @Inject constructor() : DataMapper<Movie, MovieResponse>() {

    override fun transformToEntity(model: Movie): MovieResponse? {
        // Unnecessary transform
        return null
    }

    override fun transformToModel(entity: MovieResponse): Movie? {
        return Movie(
            entity.adult,
            entity.backdrop_path,
            entity.genre_ids,
            entity.id,
            entity.original_language,
            entity.original_title,
            entity.overview,
            entity.popularity,
            entity.poster_path,
            entity.release_date,
            entity.title,
            entity.video,
            entity.vote_average,
            entity.vote_count
        )
    }
}
