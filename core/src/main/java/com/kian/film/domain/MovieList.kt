package com.kian.film.domain

data class MovieList(
    val dates: Dates?,
    val page: Int,
    val movies: List<Movie>,
    val totalPages: Int,
    val totalResults: Int
)
