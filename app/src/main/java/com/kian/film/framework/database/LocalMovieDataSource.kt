package com.kian.film.framework.database

import android.content.Context
import com.kian.film.data.MovieDataSource
import com.kian.film.domain.MovieDetails
import com.kian.film.domain.MovieList
import com.kian.film.domain.Resource

class LocalMovieDataSource(context: Context) : MovieDataSource {

    override suspend fun getNowPlayingMovies(page: Int): Resource<MovieList> {
        TODO("Not yet implemented")
    }

    override suspend fun getPopularMovies(page: Int): Resource<MovieList> {
        TODO("Not yet implemented")
    }

    override suspend fun getTopRatedMovies(page: Int): Resource<MovieList> {
        TODO("Not yet implemented")
    }

    override suspend fun getUpcomingMovies(page: Int): Resource<MovieList> {
        TODO("Not yet implemented")
    }

    override suspend fun getMovieDetails(movieId: Int): Resource<MovieDetails> {
        TODO("Not yet implemented")
    }
}
