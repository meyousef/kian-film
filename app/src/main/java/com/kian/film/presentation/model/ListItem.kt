package com.kian.film.presentation.model

import com.kian.film.domain.Movie

sealed class ListItem(val viewType: ListItemType) {

    data class MovieListItem(val movie: Movie) : ListItem(ListItemType.MOVIE_ITEM)

    data class LoadingListItem(val page: Int) : ListItem(ListItemType.LOADING_ITEM)

    data class ErrorListItem(
        val errorMsg: String,
        val page: Int
    ) : ListItem(ListItemType.ERROR_ITEM)
}
