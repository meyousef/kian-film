package com.kian.film.framework.network.response

import com.google.gson.annotations.SerializedName

data class CountryResponse(
    @SerializedName("iso_3166_1")
    val iso_3166_1: String,

    @SerializedName("name")
    val name: String
)
