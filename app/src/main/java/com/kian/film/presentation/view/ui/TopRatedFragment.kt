package com.kian.film.presentation.view.ui

import android.content.Context
import android.os.Bundle
import android.view.*
import android.view.animation.AnimationUtils
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.kian.film.common.BaseFragment
import com.kian.film.R
import com.kian.film.common.help.EndlessScrollListener
import com.kian.film.common.help.observeNonNull
import com.kian.film.common.help.runIfNull
import com.kian.film.databinding.FragmentTopRatedBinding
import com.kian.film.presentation.view.adapter.MovieListAdapter
import com.kian.film.presentation.view.adapter.VerticalSpaceItemDecoration
import com.kian.film.presentation.viewmodel.MainVM
import com.kian.film.presentation.viewstate.MovieListVS
import javax.inject.Inject

class TopRatedFragment : BaseFragment() {

    /**
     * Values
     */

    @Inject
    internal lateinit var viewModelProviderFactory: ViewModelProvider.Factory

    private lateinit var mainVM: MainVM
    private lateinit var dataBinding: FragmentTopRatedBinding
    private lateinit var moviesAdapter: MovieListAdapter

    /****************************************************
     * FRAGMENT LIFECYCLE
     ***************************************************/

    override fun onAttach(context: Context) {
        super.onAttach(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mainVM = ViewModelProvider(requireActivity(), viewModelProviderFactory)[MainVM::class.java]
        savedInstanceState.runIfNull {
            fetchNextMoviesPage(FIRST_PAGE)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        parent: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        dataBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_top_rated, parent, false)

        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onCreateContextMenu(
        menu: ContextMenu,
        v: View,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        super.onCreateContextMenu(menu, v, menuInfo)
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        return false
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onStop() {
        super.onStop()
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onDetach() {
        super.onDetach()
    }

    /****************************************************
     * VIEW/DATA BINDING
     ***************************************************/

    override fun setupViews() {
        setupMovieListView()
        setupErrorAnnounce()
    }

    override fun setupMovieListView() {
        moviesAdapter = MovieListAdapter(mCallback)

        val linearLayoutManager = LinearLayoutManager(context)
        dataBinding.movies.apply {
            adapter = moviesAdapter
            layoutManager = linearLayoutManager
            addItemDecoration(VerticalSpaceItemDecoration(1))
            addOnScrollListener(object : EndlessScrollListener(linearLayoutManager) {
                override fun onLoadMore(page: Int) {
                    fetchNextMoviesPage(page)
                }
            })
        }

        dataBinding.swipeContainer.setOnRefreshListener {
            moviesAdapter.clearItems()
            fetchNextMoviesPage(FIRST_PAGE)
        }
    }

    override fun setupErrorAnnounce() {
        dataBinding.errorAnnounce.setFactory {
            val t = TextView(context)
            t.gravity = Gravity.CENTER_VERTICAL or Gravity.CENTER_HORIZONTAL
            t.setTextColor(context!!.resources.getColor(R.color.colorTextDescription))
            t
        }
        dataBinding.errorAnnounce.inAnimation = AnimationUtils.loadAnimation(
            context,
            R.anim.slide_in_down
        )
        dataBinding.errorAnnounce.outAnimation = AnimationUtils.loadAnimation(
            context,
            R.anim.slide_out_up
        )
        dataBinding.errorAnnounce.setCurrentText("")
    }

    override fun renderMovieListViewState(movieListVS: MovieListVS) {
        with(dataBinding) {
            viewState = movieListVS
            executePendingBindings()
        }

        // Handle showing loading indicator for sequence pages
        movieListVS.getLoadingListItem()?.let {
            moviesAdapter.showLoadingItem(it)
        } ?: run {
            moviesAdapter.hideLoadingItem()
        }

        // Handle showing error for initial page
        if (movieListVS.shouldShowErrorAnnounce()) {
            dataBinding.errorAnnounce.setText(movieListVS.getErrorMessage())
        }
        else {
            dataBinding.errorAnnounce.setText("")
        }

        // Handle showing error for sequence pages
        movieListVS.getErrorListItem()?.let {
            moviesAdapter.showErrorItem(it)
        } ?: run {
            moviesAdapter.hideErrorItem()
        }

        // Handle showing movies
        moviesAdapter.addItems(movieListVS.getMovieListItems())
    }

    override fun fetchNextMoviesPage(page: Int) {
        mainVM.fetchTopRatedMovies(page)
    }

    /****************************************************
     * OBSERVERS
     ***************************************************/

    override fun setupObservers() {
        super.setupObservers()

        mainVM.getTopRatedMoviesStream().observeNonNull(viewLifecycleOwner) {
            renderMovieListViewState(it)
        }
    }

    companion object {

        /**
         * Use this factory method to create a new instance of this fragment.
         *
         * @return A new instance of fragment [TopRatedFragment].
         */
        fun newInstance(): TopRatedFragment {
            return TopRatedFragment()
        }

        val TAG = TopRatedFragment::class.java.simpleName
        val ID = TopRatedFragment::class.java.simpleName.hashCode()
        const val TITLE: Int = R.string.top_rated_fragment_label
    }
}
