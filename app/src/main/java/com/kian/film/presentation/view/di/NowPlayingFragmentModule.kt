package com.kian.film.presentation.view.di

import androidx.fragment.app.FragmentManager
import com.kian.film.framework.di.scopes.FragmentScope
import com.kian.film.presentation.view.ui.NowPlayingFragment
import dagger.Module
import dagger.Provides

@Module
class NowPlayingFragmentModule {

    @Provides
    @FragmentScope
    fun provideFragmentManager(fragment: NowPlayingFragment): FragmentManager {
        return fragment.childFragmentManager
    }

    // TODO("Provide other fragment dependencies here...")
}
