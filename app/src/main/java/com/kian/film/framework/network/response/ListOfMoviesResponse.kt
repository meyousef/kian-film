package com.kian.film.framework.network.response

import com.google.gson.annotations.SerializedName

data class ListOfMoviesResponse(
    @SerializedName("dates")
    val dates: DatesResponse?,

    @SerializedName("page")
    val page: Int,

    @SerializedName("results")
    val movies: List<MovieResponse>,

    @SerializedName("total_pages")
    val total_pages: Int,

    @SerializedName("total_results")
    val total_results: Int
)
