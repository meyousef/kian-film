package com.kian.film.framework.di.components

import android.app.Application
import com.kian.film.App
import com.kian.film.framework.di.modules.ActivityBuilderModule
import com.kian.film.framework.di.modules.DataSourceModule
import com.kian.film.framework.di.modules.FragmentBuilderModule
import com.kian.film.framework.di.modules.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ActivityBuilderModule::class,
        FragmentBuilderModule::class,
        ViewModelModule::class,
        DataSourceModule::class
    ]
)
interface AppComponent : AndroidInjector<App> {

    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<App>() {

        @BindsInstance
        abstract fun app(application: Application): Builder
    }
}
