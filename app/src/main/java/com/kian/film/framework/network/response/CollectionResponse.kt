package com.kian.film.framework.network.response

import com.google.gson.annotations.SerializedName

data class CollectionResponse(
    @SerializedName("backdrop_path")
    val backdrop_path: String?,

    @SerializedName("id")
    val id: Int,

    @SerializedName("name")
    val name: String,

    @SerializedName("poster_path")
    val poster_path: String?
)
