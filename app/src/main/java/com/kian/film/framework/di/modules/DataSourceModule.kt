package  com.kian.film.framework.di.modules

import com.kian.film.data.MovieDataSource
import com.kian.film.framework.database.DatabaseModule
import com.kian.film.framework.network.NetworkModule
import com.kian.film.framework.network.RemoteMovieDataSource
import com.kian.film.framework.network.RemoteMovieService
import com.kian.film.framework.network.mapper.DetailsOfMovieResponseMapper
import com.kian.film.framework.network.mapper.ListOfMoviesResponseMapper
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [DatabaseModule::class, NetworkModule::class])
class DataSourceModule {

    @Provides
    @Singleton
    fun provideRemoteMovieDataSource(
        service: RemoteMovieService,
        listOfMoviesResponseMapper: ListOfMoviesResponseMapper,
        detailsOfMovieResponseMapper: DetailsOfMovieResponseMapper
    ): MovieDataSource {
        return RemoteMovieDataSource(service, listOfMoviesResponseMapper, detailsOfMovieResponseMapper)
    }
}
