package com.kian.film.framework.di.qualifiers

import javax.inject.Qualifier

@Qualifier
annotation class ActivityContext
