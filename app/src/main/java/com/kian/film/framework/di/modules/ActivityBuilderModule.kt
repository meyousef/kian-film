package com.kian.film.framework.di.modules

import com.kian.film.framework.di.scopes.ActivityScope
import com.kian.film.presentation.view.di.MainActivityModule
import com.kian.film.presentation.view.di.MovieDetailsActivityModule
import com.kian.film.presentation.view.ui.MainActivity
import com.kian.film.presentation.view.ui.MovieDetailsActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilderModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = [MainActivityModule::class])
    abstract fun bindMainActivity(): MainActivity


    @ActivityScope
    @ContributesAndroidInjector(modules = [MovieDetailsActivityModule::class])
    abstract fun bindMovieDetailsActivity(): MovieDetailsActivity
}
