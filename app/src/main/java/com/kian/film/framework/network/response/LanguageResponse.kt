package com.kian.film.framework.network.response

import com.google.gson.annotations.SerializedName

data class LanguageResponse(
    @SerializedName("iso_639_1")
    val iso_639_1: String,

    @SerializedName("name")
    val name: String
)
