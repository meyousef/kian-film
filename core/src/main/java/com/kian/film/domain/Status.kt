package com.kian.film.domain

// references :
// https://developer.android.com/jetpack/docs/guide#addendum

enum class Status {

    LOADING,
    SUCCESS,
    ERROR
}
