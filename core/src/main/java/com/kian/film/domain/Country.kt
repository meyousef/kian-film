package com.kian.film.domain

data class Country(
    val iso31661: String,
    val name: String
)
