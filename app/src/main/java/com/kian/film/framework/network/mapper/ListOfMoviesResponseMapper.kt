package com.kian.film.framework.network.mapper

import com.kian.film.domain.MovieList
import com.kian.film.mapper.DataMapper
import com.kian.film.framework.network.response.ListOfMoviesResponse
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ListOfMoviesResponseMapper @Inject constructor(
    private val datesResponseMapper: DatesResponseMapper,
    private val movieResponseMapper: MovieResponseMapper
) : DataMapper<MovieList, ListOfMoviesResponse>() {

    override fun transformToEntity(model: MovieList): ListOfMoviesResponse? {
        // Unnecessary transform
        return null
    }

    override fun transformToModel(entity: ListOfMoviesResponse): MovieList? {
        return MovieList(
            entity.dates?.let { datesResponseMapper.transformToModel(it) },
            entity.page,
            movieResponseMapper.transformToModels(entity.movies),
            entity.total_pages,
            entity.total_results
        )
    }
}
