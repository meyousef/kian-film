package com.kian.film.interactors

import com.kian.film.data.MovieRepository
import com.kian.film.domain.MovieDetails
import com.kian.film.domain.Resource
import javax.inject.Inject

class GetMovieDetails @Inject constructor(private val movieRepository: MovieRepository) {

    suspend operator fun invoke(movieId: Int): Resource<MovieDetails> =
        movieRepository.getMovieDetails(movieId)
}
