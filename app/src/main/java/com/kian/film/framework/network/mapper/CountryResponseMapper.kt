package com.kian.film.framework.network.mapper

import com.kian.film.domain.Country
import com.kian.film.mapper.DataMapper
import com.kian.film.framework.network.response.CountryResponse
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CountryResponseMapper @Inject constructor() : DataMapper<Country, CountryResponse>() {

    override fun transformToEntity(model: Country): CountryResponse? {
        // Unnecessary transform
        return null
    }

    override fun transformToModel(entity: CountryResponse): Country? {
        return Country(
            entity.iso_3166_1,
            entity.name
        )
    }
}
