package com.kian.film

object Const {

    // The base URL used for TMDB API services.
    const val BASE_URL: String = "https://api.themoviedb.org/3/"

    // The API Key required for TMDB API services.
    const val API_KEY = "e43727aea2e271ba36c703e1b66ef71f"

    // The language specification for TMDB API services.
    const val LANGUAGE = "en-US"

    /* Bundled Extra Queries */
    const val EXTRA_MOVIE_ID = "EXTRA_MOVIE_ID"
    const val EXTRA_MOVIE_NAME = "EXTRA_MOVIE_NAME"
}