package com.kian.film.presentation.view.di

import androidx.fragment.app.FragmentManager
import com.kian.film.framework.di.scopes.FragmentScope
import com.kian.film.presentation.view.ui.PopularFragment
import dagger.Module
import dagger.Provides

@Module
class PopularFragmentModule {

    @Provides
    @FragmentScope
    fun provideFragmentManager(fragment: PopularFragment): FragmentManager {
        return fragment.childFragmentManager
    }

    // TODO("Provide other fragment dependencies here...")
}
