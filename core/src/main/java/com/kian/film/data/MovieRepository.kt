package com.kian.film.data

import com.kian.film.domain.MovieDetails
import com.kian.film.domain.MovieList
import com.kian.film.domain.Resource
import javax.inject.Inject

class MovieRepository @Inject constructor(private val dataSource: MovieDataSource) {

    suspend fun getNowPlayingMovies(page: Int): Resource<MovieList> =
        dataSource.getNowPlayingMovies(page)

    suspend fun getPopularMovies(page: Int): Resource<MovieList> =
        dataSource.getPopularMovies(page)

    suspend fun getTopRatedMovies(page: Int): Resource<MovieList> =
        dataSource.getTopRatedMovies(page)

    suspend fun getUpcomingMovies(page: Int): Resource<MovieList> =
        dataSource.getUpcomingMovies(page)

    suspend fun getMovieDetails(movieId: Int): Resource<MovieDetails> =
        dataSource.getMovieDetails(movieId)
}
