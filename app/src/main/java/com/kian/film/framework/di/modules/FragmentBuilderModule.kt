package com.kian.film.framework.di.modules

import com.kian.film.framework.di.scopes.FragmentScope
import com.kian.film.presentation.view.di.NowPlayingFragmentModule
import com.kian.film.presentation.view.di.PopularFragmentModule
import com.kian.film.presentation.view.di.TopRatedFragmentModule
import com.kian.film.presentation.view.di.UpcomingFragmentModule
import com.kian.film.presentation.view.ui.NowPlayingFragment
import com.kian.film.presentation.view.ui.PopularFragment
import com.kian.film.presentation.view.ui.TopRatedFragment
import com.kian.film.presentation.view.ui.UpcomingFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuilderModule {

    @FragmentScope
    @ContributesAndroidInjector(modules = [NowPlayingFragmentModule::class])
    abstract fun bindNowPlayingFragment(): NowPlayingFragment

    @FragmentScope
    @ContributesAndroidInjector(modules = [PopularFragmentModule::class])
    abstract fun bindPopularFragment(): PopularFragment

    @FragmentScope
    @ContributesAndroidInjector(modules = [TopRatedFragmentModule::class])
    abstract fun bindTopRatedFragment(): TopRatedFragment

    @FragmentScope
    @ContributesAndroidInjector(modules = [UpcomingFragmentModule::class])
    abstract fun bindUpcomingFragment(): UpcomingFragment
}
