package com.kian.film.common.help

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.kian.film.R

/**
 * This is just a copy-paste class!!
 */

object ImageBindingAdapter {

    @JvmStatic
    @BindingAdapter("thumbnailImgUrl")
    fun setThumbnailImgUrl(imageView: ImageView, imageUrl: String?) {
        Glide.with(imageView.context)
            .load("https://image.tmdb.org/t/p/w500/$imageUrl")
            .placeholder(R.mipmap.movie_poster_default_now_playing)
            .into(imageView)
    }

    @JvmStatic
    @BindingAdapter("originalImgUrl")
    fun setOriginalImgUrl(imageView: ImageView, imageUrl: String?) {
        Glide.with(imageView.context)
            .load("https://image.tmdb.org/t/p/w500/$imageUrl")
            .placeholder(R.mipmap.movie_card_default)
            .into(imageView)
    }

    @JvmStatic
    @BindingAdapter("visibleIf")
    fun changeVisibility(view: View, visible: Boolean) {
        view.visibility = if (visible) View.VISIBLE else View.GONE
    }
}
