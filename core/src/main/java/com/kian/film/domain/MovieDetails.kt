package com.kian.film.domain

data class MovieDetails(
    val adult: Boolean?,
    val backdropPath: String?,
    val belongsToCollection: Collection?,
    val budget: Int?,
    val genres: List<Genre>?,
    val homepage: String?,
    val id: Int,
    val imdbId: String?,
    val originalLanguage: String?,
    val originalTitle: String,
    val overview: String,
    val popularity: Double?,
    val posterPath: String?,
    val productionCompanies: List<Company>?,
    val productionCountries: List<Country>?,
    val release_date: String?,
    val revenue: Int?,
    val runtime: Int?,
    val spokenLanguages: List<Language>?,
    val status: String?,
    val tagline: String?,
    val title: String,
    val video: Boolean?,
    val voteAverage: Double?,
    val voteCount: Int?
)
