package com.kian.film.domain

data class Collection(
    val backdropPath: String?,
    val id: Int,
    val name: String,
    val posterPath: String?
)
