package com.kian.film.framework.di.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.kian.film.common.help.ViewModelFactory
import com.kian.film.framework.di.keys.ViewModelKey
import com.kian.film.presentation.viewmodel.MainVM
import com.kian.film.presentation.viewmodel.MovieDetailsVM
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(viewModelFactory: ViewModelFactory): ViewModelProvider.Factory

    @IntoMap
    @Binds
    @ViewModelKey(MainVM::class)
    abstract fun provideMainViewModel(mainVM: MainVM): ViewModel

    @IntoMap
    @Binds
    @ViewModelKey(MovieDetailsVM::class)
    abstract fun provideMovieDetailsViewModel(movieDetailsVM: MovieDetailsVM): ViewModel
}
