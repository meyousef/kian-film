package com.kian.film.presentation.view.di

import androidx.fragment.app.FragmentManager
import com.kian.film.framework.di.scopes.ActivityScope
import com.kian.film.presentation.view.ui.*
import dagger.Module
import dagger.Provides

@Module
class MovieDetailsActivityModule() {

    @Provides
    @ActivityScope
    fun provideFragmentManager(activity: MovieDetailsActivity): FragmentManager {
        return activity.supportFragmentManager
    }

    // TODO("Provide other activity dependencies here...")
}
