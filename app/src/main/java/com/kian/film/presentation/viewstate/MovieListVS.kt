package com.kian.film.presentation.viewstate

import com.kian.film.domain.Status
import com.kian.film.presentation.model.ListItem

class MovieListVS(
    private val status: Status,
    private val errorMessage: String? = null,
    private val movieListItems: List<ListItem.MovieListItem>? = null,
    val page: Int = 1
) {

    fun isLoadingInitialPage() = status == Status.LOADING && page == 1

    fun isLoadingSequencePages() = status == Status.LOADING && page > 1

    fun shouldShowErrorAnnounce() = errorMessage != null && page == 1

    fun shouldShowErrorToast() = errorMessage != null && page > 1

    fun getErrorMessage() = errorMessage

    fun getMovieListItems() = movieListItems ?: mutableListOf()

    fun getLoadingListItem() = if (isLoadingSequencePages()) {
        ListItem.LoadingListItem(page)
    }
    else {
        null
    }

    fun getErrorListItem() = if (shouldShowErrorToast()) {
        ListItem.ErrorListItem(errorMessage!!, page)
    }
    else {
        null
    }
}
