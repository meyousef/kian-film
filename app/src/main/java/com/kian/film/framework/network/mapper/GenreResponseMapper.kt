package com.kian.film.framework.network.mapper

import com.kian.film.domain.Genre
import com.kian.film.mapper.DataMapper
import com.kian.film.framework.network.response.GenreResponse
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GenreResponseMapper @Inject constructor() : DataMapper<Genre, GenreResponse>() {

    override fun transformToEntity(model: Genre): GenreResponse? {
        // Unnecessary transform
        return null
    }

    override fun transformToModel(entity: GenreResponse): Genre? {
        return Genre(
            entity.id,
            entity.name
        )
    }
}
