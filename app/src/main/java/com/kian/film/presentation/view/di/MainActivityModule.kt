package com.kian.film.presentation.view.di

import androidx.fragment.app.FragmentManager
import com.kian.film.framework.di.scopes.ActivityScope
import com.kian.film.presentation.view.ui.*
import dagger.Module
import dagger.Provides

@Module
class MainActivityModule() {

    @Provides
    @ActivityScope
    fun provideFragmentManager(activity: MainActivity): FragmentManager {
        return activity.supportFragmentManager
    }

    @Provides
    @ActivityScope
    fun provideNowPlayingFragment(): NowPlayingFragment {
        return NowPlayingFragment.newInstance()
    }

    @Provides
    @ActivityScope
    fun providePopularFragment(): PopularFragment {
        return PopularFragment.newInstance()
    }

    @Provides
    @ActivityScope
    fun provideTopRatedFragment(): TopRatedFragment {
        return TopRatedFragment.newInstance()
    }

    @Provides
    @ActivityScope
    fun provideUpcomingFragment(): UpcomingFragment {
        return UpcomingFragment.newInstance()
    }

    // TODO("Provide other activity dependencies here...")
}
