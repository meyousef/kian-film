package com.kian.film.presentation.view.ui

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModelProvider
import com.kian.film.R
import com.kian.film.common.BaseActivity
import com.kian.film.databinding.ActivityMainBinding
import com.kian.film.presentation.viewmodel.MainVM
import javax.inject.Inject

class MainActivity : BaseActivity() {

    /**
     * Values
     */

    @Inject
    internal lateinit var viewModelProviderFactory: ViewModelProvider.Factory

    private lateinit var mainVM: MainVM
    private lateinit var dataBinding: ActivityMainBinding

    @Inject
    lateinit var fragmentManager: FragmentManager

    @Inject
    lateinit var nowPlayingFragment: NowPlayingFragment

    @Inject
    lateinit var popularFragment: PopularFragment

    @Inject
    lateinit var topRatedFragment: TopRatedFragment

    @Inject
    lateinit var upcomingFragment: UpcomingFragment

    /****************************************************
     * ACTIVITY LIFECYCLE
     ***************************************************/

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
    }

    override fun onStop() {
        super.onStop()
    }

    override fun onRestart() {
        super.onRestart()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)

        extractIntentParams(intent)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)

        return true
    }

    /****************************************************
     * ACTIVITY STATE
     ***************************************************/

    override fun initializeActivity(savedInstanceState: Bundle?) {
        // Nothing
    }

    override fun extractIntentParams(data: Intent?) {
        // Nothing
    }

    /****************************************************
     * VIEW/DATA BINDING
     ***************************************************/

    override fun setupViews() {
        // Set Content View
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
    }

    override fun setupActionBar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        val actionBar = supportActionBar
        actionBar!!.setDisplayShowTitleEnabled(true)
        actionBar.setDisplayHomeAsUpEnabled(false)
    }

    override fun setupNavigation() {
        // Bottom Navigation View
        dataBinding.content.navigation.setOnNavigationItemSelectedListener { menuItem ->
            menuItem.isChecked = true

            when (menuItem.itemId) {
                R.id.now_playing -> {
                    setupNowPlayingFragment()
                    return@setOnNavigationItemSelectedListener true
                }
                R.id.popular     -> {
                    setupPopularFragment()
                    return@setOnNavigationItemSelectedListener true
                }
                R.id.top_rated   -> {
                    setupTopRatedFragment()
                    return@setOnNavigationItemSelectedListener true
                }
                R.id.upcoming    -> {
                    setupUpcomingFragment()
                    return@setOnNavigationItemSelectedListener true
                }
                else             -> {
                    return@setOnNavigationItemSelectedListener false
                }
            }
        }

        // Init fragments and then, open [NowPlayingFragment] as default fragment
        initFragments()
    }

    private fun initFragments() {
        /* Fragments will be injected by Mr. Dagger */

        // Show [NowPlayingFragment] as default fragment
        setupNowPlayingFragment()
    }

    private fun changeFragment(fragment: Fragment, tag: String) {
        val fragmentTransaction = fragmentManager.beginTransaction()

        // Hide current active fragment
        val currentFragment = fragmentManager.primaryNavigationFragment
        currentFragment?.let {
            fragmentTransaction.hide(it)
        }

        // Show/Add fragment that is candidate to be active
        var candidateFragment = fragmentManager.findFragmentByTag(tag)
        candidateFragment?.let {
            fragmentTransaction.show(it)
        } ?: run {
            candidateFragment = fragment
            fragmentTransaction.add(R.id.container, candidateFragment!!, tag)
        }

        fragmentTransaction.setPrimaryNavigationFragment(candidateFragment)
        fragmentTransaction.setReorderingAllowed(true)
        fragmentTransaction.commitNowAllowingStateLoss()
    }

    private fun setupNowPlayingFragment() {
        // Customize AppBar or other uncommon here
        this.supportActionBar!!.setTitle(R.string.app_name)

        changeFragment(nowPlayingFragment, NowPlayingFragment.TAG)
    }

    private fun setupPopularFragment() {
        // Customize AppBar or other uncommon here
        this.supportActionBar!!.setTitle(PopularFragment.TITLE)

        changeFragment(popularFragment, PopularFragment.TAG)
    }

    private fun setupTopRatedFragment() {
        // Customize AppBar or other uncommon here
        this.supportActionBar!!.setTitle(TopRatedFragment.TITLE)

        changeFragment(topRatedFragment, TopRatedFragment.TAG)
    }

    private fun setupUpcomingFragment() {
        // Customize AppBar or other uncommon here
        this.supportActionBar!!.setTitle(UpcomingFragment.TITLE)

        changeFragment(upcomingFragment, UpcomingFragment.TAG)
    }

    /****************************************************
     * OBSERVERS
     ***************************************************/

    override fun setupObservers() {
        super.setupObservers()

        mainVM = ViewModelProvider(this, viewModelProviderFactory).get(MainVM::class.java)
    }

    /****************************************************
     * SERVICE BINDING
     ***************************************************/

    // Nothing

    companion object {
        val TAG = MainActivity::class.java.simpleName
        const val EXTRA_FRAGMENT_ID = "EXTRA_FRAGMENT_ID"
        const val EXTRA_FRAGMENT_TITLE = "EXTRA_FRAGMENT_TITLE"
    }
}
