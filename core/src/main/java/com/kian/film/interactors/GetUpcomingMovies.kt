package com.kian.film.interactors

import com.kian.film.data.MovieRepository
import com.kian.film.domain.MovieList
import com.kian.film.domain.Resource
import javax.inject.Inject

class GetUpcomingMovies @Inject constructor(private val movieRepository: MovieRepository) {

    suspend operator fun invoke(page: Int) : Resource<MovieList> =
        movieRepository.getUpcomingMovies(page)
}
