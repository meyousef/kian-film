package com.kian.film.presentation.viewstate

import com.kian.film.domain.MovieDetails
import com.kian.film.domain.Status

class MovieDetailsVS(
    private val status: Status,
    private val errorMessage: String? = null,
    private val movieDetails: MovieDetails? = null
) {

    fun isLoading() = status == Status.LOADING

    fun shouldShowErrorMessage() = errorMessage != null

    fun getErrorMessage() = errorMessage

    fun getMovieDetails() = movieDetails

    /****************************************************
     * Accessory
     ***************************************************/

    val movieTitle = movieDetails?.title ?: ""

    val movieOriginalTitle = "Original Title: ${movieDetails?.originalTitle ?: "--"}"

    val movieRuntime = "Runtime: ${movieDetails?.runtime?.toString() ?: "--"}, "

    val movieVoteAverage = "Vote: ${movieDetails?.voteAverage?.toString() ?: "--"}, "

    val movieVoteCount = "Users: ${movieDetails?.voteCount?.toString() ?: "--"}"

    val movieBudget = "Budget: ${movieDetails?.budget?.toString() ?: "--"}"

    val movieGenres = "Genres: ${movieDetails?.genres?.get(0)?.name ?: "--"}"

    val movieTagline = "${movieDetails?.tagline ?: "--"}"

    val movieOverview = "${movieDetails?.overview ?: "--"}"
}
